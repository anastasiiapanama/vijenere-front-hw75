import axiosApi from "../axios-api";

export const FETCH_MESSAGE_REQUEST = 'FETCH_MESSAGE_REQUEST';
export const FETCH_MESSAGE_SUCCESS = 'FETCH_MESSAGE_SUCCESS';
export const CHANGE_HANDLER_VALUE = 'CHANGE_HANDLER_VALUE';
export const ADD_ENCODE = 'ADD_ENCODE';
export const DECODE = 'DECODE';
export const DECODE_ACT = 'DECODE_ACT';

export const changeHandlerValue = event => ({type: CHANGE_HANDLER_VALUE, event});
export const addDecode = decode => ({type: DECODE, decode});
export const addDecodeAct = encode => ({type: DECODE_ACT, encode});

export const addEncodeMessage = data => {
    return async dispatch => {
        try {
            console.log(data)
            const encode = {
                message: data.encode,
                password: data.password
            }

            const response = await axiosApi.post('/encode', encode);

            dispatch(addDecode(response.data));
        } catch (e) {
            console.log(e);
        }
    };
};

export const addDecodeMessage = data => {
    return async dispatch => {
        try {
            const decode = {
                message: data.decode,
                password: data.password
            }

            const response = await axiosApi.post('/decode', decode);

            dispatch(addDecodeAct(response.data));
        } catch (e) {
            console.log(e);
        }
    };
};