import {
    ADD_ENCODE, CHANGE_HANDLER_VALUE, DECODE, DECODE_ACT,
    FETCH_MESSAGE_REQUEST,
    FETCH_MESSAGE_SUCCESS
} from "./actions";

const initialState = {
    encode: '',
    decode: '',
    password: '',
    loading: true
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_MESSAGE_REQUEST:
            return {...state, loading: true}
        case FETCH_MESSAGE_SUCCESS:
            return {...state, loading: false}
        case CHANGE_HANDLER_VALUE:
            return {...state, [action.event.name]: action.event.value};
        case ADD_ENCODE:
            return {...state, loading: false}
        case DECODE:
            return {...state, decode: action.decode}
        case DECODE_ACT:
            return {...state, encode: action.encode}
        default:
            return state;
    }
};

export default reducer;