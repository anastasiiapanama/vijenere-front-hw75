import React from "react";
import {useDispatch, useSelector} from "react-redux";
import CssBaseline from "@material-ui/core/CssBaseline";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";
import makeStyles from "@material-ui/core/styles/makeStyles";
import Paper from "@material-ui/core/Paper";

import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import IconButton from "@material-ui/core/IconButton";
import {addDecodeMessage, addEncodeMessage, changeHandlerValue} from "./store/actions";

const useStyles = makeStyles(theme => ({
    paper: {
        padding: theme.spacing(2)
    }
}));

const App = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const state = useSelector(state => state);

    const changeHandler = event => {
        dispatch(changeHandlerValue(event.target));
    };

    const handlerDecode = event => {
        event.preventDefault();

        dispatch(addDecodeMessage(state));
    };

    const handlerEncode = event => {
        event.preventDefault();

        dispatch(addEncodeMessage(state));
    };

    return (
        <>
            <CssBaseline/>
            <Paper className={classes.paper}>
                <Grid container direction="column" spacing={2}>
                    <Grid container item xs>
                        <Typography variant="h5" align="center">Decoded message</Typography>

                        <TextField
                            variant="outlined"
                            fullWidth
                            type="content"
                            name="encode"
                            value={state.encode}
                            required
                            onChange={e => changeHandler(e)}
                            multiline
                            rows={2}
                        />
                    </Grid>
                    <Grid container item xs alignItems="center">
                        <Grid item xs={2}>
                            <Typography variant="h5" align="center">Password</Typography>
                        </Grid>

                        <Grid item xs={6}>
                            <TextField
                                variant="outlined"
                                fullWidth
                                type="content"
                                name="password"
                                value={state.password}
                                required
                                onChange={e => changeHandler(e)}
                                multiline
                                rows={1}
                            />
                        </Grid>

                        <Grid item xs>
                            <IconButton aria-label="arrow-down" onClick={handlerEncode}>
                                <KeyboardArrowDownIcon/>
                            </IconButton>
                            <IconButton aria-label="arrow-up" onClick={handlerDecode}>
                                <KeyboardArrowUpIcon/>
                            </IconButton>
                        </Grid>
                    </Grid>
                    <Grid container item xs>
                        <Typography variant="h5" align="center">Encoded message</Typography>

                        <TextField
                            variant="outlined"
                            fullWidth
                            type="content"
                            name="decode"
                            value={state.decode}
                            required
                            onChange={e => changeHandler(e)}
                            multiline
                            rows={2}
                        />
                    </Grid>
                </Grid>
            </Paper>
        </>
    )
};

export default App;
